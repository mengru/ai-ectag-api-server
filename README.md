# ai-api-server-template

ai-api-server-template description

## Before Setup

### Project Renaming

1. search all files and strings contains `ai-new-api-server` and `ai_new_api_server`
2. replace them into `ai-{project}-api-server` and `ai_{project}_api_server`

```
ai_new_api_server/
MANIFEST.in
Makefile (also assign a new port, default 5566)
ai-new-api-server.conf (also assign a new port, default 5566)
setup.py
ansible/deploy.yml
```

3. make your own endpoints (and document) by referencing following examples:

```
ai_new_api_server/refund.py
builder/refund_query_builder.py
query_agent/refund_query_agent.py
api_docs/refund/get_refund_candidates.yml
```

and remove these example files after you finished your endpoints

## Quick Start

Run the application:

    make run

And open it in the browser at [http://127.0.0.1:5678/](http://127.0.0.1:5678/)
API documents is at [http://127.0.0.1:5678/apidocs](http://127.0.0.1:5678/apidocs)


## Prerequisites

This is built to be used with Python 3. Update `Makefile` to switch to Python 2 if needed.

Some Flask dependencies are compiled during installation, so `gcc` and Python header files need to be present.
For example, on Ubuntu:

    apt install build-essential python3-dev


## Development 

 Create virtualenv with Flask and ai_optimizer_api_server installed into it (latter is installed in
   [develop mode](http://setuptools.readthedocs.io/en/latest/setuptools.html#development-mode) which allows
   modifying source code directly without a need to re-install the app)

 - configuration: create file `settings.cfg` under `${PROJECT_ROOT}`.
   Settings in `settings.cfg` will override settings in `${PROJECT_ROOT}/api_new_api_server/default_settings.py`,
   so write passwords and credentials in `setting.cfg`.
 
 - run development server in debug mode: `make run`.
   It will create a virtual environment with ai_optimizer_api_server installed.
   
   For development, simply re-run `make run` after modifying codes.

 - run tests: TODO (see also: [Testing Flask Applications](http://flask.pocoo.org/docs/0.12/testing/))

 - build package (including install requirements): `make build`.
   It will create `ai_optimizer_api_server-xx.tar.gz` in `dist/` folder.
   You can skip this step if using `make run` and do not need the package.

 - to remove virtualenv and built distributions: `make clean`
 
 - to add more python dependencies: add to `requirements.txt`

## Deploy to staging and production
 
 - configuration: put a proper configuration file to staging/production environment 
   (`/etc/ai-optimizer-api-server/settings.cfg`).
   
 - deploy to staging: handle by Jenkins 
   [ai_optimizer_api_server-build_stg](http://ai-ci.tw.appier.biz:8080/job/ai_optimizer_api_server-build_stg).
   It will check master branch periodically 
   and run `ansible/deploy.yml` to deploy to staging server if changes found.
 
 - deploy to production: handle by Jenkins
   [ai_optimizer_api_server-build_prod](http://ai-ci.tw.appier.biz:8080/job/ai_optimizer_api_server-build_prod).
   Trigger the job manually and it will run `ansible/deploy.yml` to deploy to production server.
   

## Deployment

The deployment steps is written in `ansible/deploy.tml`. 

Generally the idea is to build a package (`make buld`), deliver it to a server (`scp ...`),
install it (`pip install ai_optimizer_api_server.tar.gz`), ensure that configuration file exists and
`API_SETTINGS` environment variable points to it, ensure that user has access to the
working directory to create and write log files in it, and finally run a
[WSGI container](http://flask.pocoo.org/docs/0.12/deploying/wsgi-standalone/) with the application.
Most likely it will also run behind a
[reverse proxy](http://flask.pocoo.org/docs/0.12/deploying/wsgi-standalone/#proxy-setups).

Check out [Deploying with Fabric](http://flask.pocoo.org/docs/0.12/patterns/fabric/#fabric-deployment) on one of the
possible ways to automate the deployment.
