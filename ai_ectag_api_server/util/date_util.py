import calendar
from datetime import datetime, timedelta


def date_str_interval_to_ts(start_date_str, end_date_str, tz):
    """
    :param start_date_str: string of start date, e.g. 20180101
    :param end_date_str:string of end date, e.g. 20180101
    :param tz: timezone, e.g. 8
    :return: tuple of timestamp e.g. (1514736000, 1514822400)
    """
    start_date = datetime.strptime(start_date_str, '%Y%m%d') - timedelta(hours=tz)
    start_ts = calendar.timegm(start_date.timetuple())
    # date is end-included
    end_date = datetime.strptime(end_date_str, '%Y%m%d') - timedelta(hours=tz) + timedelta(days=1)
    end_ts = calendar.timegm(end_date.timetuple())
    return start_ts, end_ts
