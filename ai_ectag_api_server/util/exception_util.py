
##############################################################
#   Exception
##############################################################


# customized exception
class CustomizedException(Exception):
    """Define Exception for Flink API Error

    A base customized exception class.

    Attributes:
        _value: A string of error message.
    """

    def __init__(self, value):
        super(CustomizedException, self).__init__(value)
        self._value = value

    def __str__(self):
        return repr(self._value)


class MySQLError(CustomizedException):
    """Define Exception for mySQL

    MySQLError will be raised if mySQL error happened.

    Attributes: None
    """

    def __init__(self, value):
        super(MySQLError, self).__init__(value)
