# -*- coding: utf-8 -*-

from flask import Flask
from flask_compress import Compress
from flask_cors import CORS
from flasgger import Swagger
from ai_ectag_api_server import mysql_util


app = Flask(__name__)
Compress(app)
CORS(app)

app.config.from_object('ai_ectag_api_server.default_settings')
app.config.from_envvar('API_SETTINGS')  # settings.cfg will overwrite the default_settings


mysql_util.init_ai_ectag_db(
    app=app,
    host=app.config.get('AI_ECTAG_HOST'),
    port=app.config.get('AI_ECTAG_PORT'),
    user=app.config.get('AI_ECTAG_USER'),
    password=app.config.get('AI_ECTAG_PASSWORD'),
    database=app.config.get('AI_ECTAG_DATABASE'),
    table=app.config.get('AI_ECTAG_INPUT_JOB_TABLE'),
    allow_modification=app.config.get('ALLOW_MODIFICATION', False)
)

if not app.debug:
    import logging
    from logging.handlers import TimedRotatingFileHandler

    # https://docs.python.org/3.6/library/logging.handlers.html#timedrotatingfilehandler
    file_handler = TimedRotatingFileHandler('ai_ectag_api_server.log', 'midnight')
    file_handler.setLevel(logging.WARNING)
    file_handler.setFormatter(logging.Formatter('<%(asctime)s> <%(levelname)s> %(message)s'))
    app.logger.addHandler(file_handler)

from ai_ectag_api_server.idash_interface import ectag
app.register_blueprint(ectag)
from ai_ectag_api_server.server_status import server_status
app.register_blueprint(server_status)

if app.config.get('PRODUCTION') is not True:
    description = '\n\n'.join([
        '* item1',
        '  * item1-1',
        '- item2',
    ])
    template = {
        "swagger": "2.0",
        "info": {
            "title": "Title of API documents",
            "description": description,
            "version": "0.0.1"
        },
    }

    swagger = Swagger(app, template=template)
