"""
1. dynamically bind parameters when needed
2. log complete query string via mysql dialect
"""
import logging
from ai_ectag_api_server import mysql_util
from ai_ectag_api_server.util.exception_util import MySQLError

logger = logging.getLogger('save_command_agent')


class SaveCommandAgent(object):
    def __init__(self):
        self.mysql_agent = mysql_util.get_ai_ectag_db_agent()
        self.column_type = None
        self.all_columns = None

    def set_columns(self):
        query = 'select column_name, data_type from information_schema.columns ' \
                'where table_schema = "{}" and table_name = "{}"'\
            .format(self.mysql_agent.database, self.mysql_agent.table)
        try:
            result = self.mysql_agent.get_query_result(query)
            if not result:
                raise MySQLError('Failed to get columns. error: empty result.')
            for idx, element in enumerate(result):
                if element['column_name'] == 'updated_at':
                    del result[idx]
                    break
            self.column_type = {col['column_name']: col['data_type'] for col in result}
            self.all_columns = sorted(self.column_type.keys())
        except MySQLError as e:
            error_msg = 'Failed to get columns. error_msg:{}'.format(str(e))
            logger.exception(error_msg)
            raise MySQLError(error_msg)

    def get_column_values(self, list_object):
        vals = []
        for col in self.all_columns:
            data_type = self.column_type[col]
            if data_type == 'varchar':
                value = list_object.get(col, '')
            elif data_type == 'int':
                value = list_object.get(col, None)
            elif data_type == 'tinyint':
                value = list_object.get(col, 0)
            elif data_type == 'timestamp':
                value = list_object.get(col, '')
            else:
                value = ''
                logger.error('unknown column type ' + data_type)
            vals.append(value)
        return vals

    def save_command_configurations(self, config_data):
        try:
            self.set_columns()
        except MySQLError as e:
            logger.exception(str(e))
            return
        vals = self.get_column_values(config_data)
        self.mysql_agent.insert_query(self.all_columns, vals)
