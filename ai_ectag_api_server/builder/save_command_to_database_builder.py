"""
builders will get output from query agent, process it to certain format, and return to endpoint
"""
from ai_ectag_api_server.query_agent.save_command_to_database_agent import SaveCommandAgent

save_command_agent = SaveCommandAgent()


def save_command(created_by, command, time_str):
    config_data = dict()

    # table field
    config_data['site_id'] = command['site_id']
    config_data['country'] = command['country'].lower()
    config_data['target_event'] = command['target_event']
    config_data['category'] = command['category']
    config_data['num_attemps'] = 0
    config_data['created_by'] = created_by
    config_data['created_at'] = time_str

    # save command configurations into database
    save_command_agent.save_command_configurations(config_data)
