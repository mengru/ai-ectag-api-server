# -*- coding: utf-8 -*-

import logging
import datetime

from flask import Blueprint, jsonify, request
from flasgger.utils import swag_from

from ai_ectag_api_server import app
from ai_ectag_api_server.builder import save_command_to_database_builder
from ai_ectag_api_server.util.exception_util import MySQLError


logging.basicConfig(level=logging.INFO)

_blueprint_name = 'ectag'
_api_doc_dir = 'api_docs/' + _blueprint_name
ectag = Blueprint(_blueprint_name, __name__, url_prefix='/ec_tagging')


@ectag.route('/create_job', methods=['POST'])
@swag_from(_api_doc_dir + '/save_idash_input_config.yml')
def ec_tagging_create_job_endpoint():
    params = request.get_json()
    results = {}
    try:
        logging.info(params)

        # required
        created_by = params['created_by']
        command = params['command']

        # save iDash input into database
        time_str = datetime.datetime.utcnow().strftime('%Y-%m-%d %X')
        save_command_to_database_builder.save_command(created_by, command, time_str)
        results['source'] = params
    except MySQLError as e:
        error_msg = 'Exception in /ec_tagging_create_job_endpoint: {}'.format(str(e))
        app.logger.exception(error_msg)
        results['error_message'] = error_msg

    return jsonify(results)
