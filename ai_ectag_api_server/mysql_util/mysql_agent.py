# -*- coding: utf-8 -*-

import pymysql

from ai_ectag_api_server.util.exception_util import MySQLError


class MySQLAgent(object):
    def __init__(self, app=None, host=None, port=None, user=None, password=None, database=None,
                 table=None, allow_modification=False):
        self.app = app
        self.host = host
        self.port = port
        self.user = user
        self.password = password
        self.database = database
        self.table = table
        self.allow_modification = allow_modification

    def get_mysql_connection(self):
        return pymysql.connect(
            host=self.host,
            port=self.port,
            user=self.user,
            passwd=self.password,
            charset='utf8',
            db=self.database
        )

    def execute_query(self, query, flatten_vals=None, retry=3):
        if not self.allow_modification:
            warning_msg = 'Query no effect: DB %s is not allowed for modification.  Check .ini file'
            self.app.logger.warn(warning_msg, self.database)
            raise MySQLError(warning_msg % self.database)

        success = False
        while retry > 0 and not success:
            try:
                connection = self.get_mysql_connection()
                cursor = connection.cursor(pymysql.cursors.DictCursor)
                cursor.execute(query, flatten_vals)
                cursor.close()
                connection.commit()
                connection.close()
                success = True
            except Exception as e:
                retry -= 1
                self.app.logger.warning('execute mysql query fail : %s, error = %s, retry = %d', query, str(e), retry)

        if not success:
            error_msg = 'execute mysql query fail : %s'
            self.app.logger.error(error_msg, query)
            raise MySQLError(error_msg % query)

    def get_query_result(self, query, flatten_vals=None, retry=3):
        success = False
        result = None
        mysql_error_msg = ''
        while retry > 0 and not success:
            try:
                connection = self.get_mysql_connection()
                cursor = connection.cursor(pymysql.cursors.DictCursor)
                cursor.execute(query, flatten_vals)

                result = []
                for r in cursor.fetchall():
                    result.append(r)
                connection.close()
                success = True
            except Exception as e:
                retry -= 1
                mysql_error_msg = str(e)
                self.app.logger.warning('execute mysql query fail : %s, error = %s, retry = %d', query, str(e), retry)

        if not success:
            error_msg = 'execute mysql query fail : {}, mysql error: {}'.format(query, mysql_error_msg)
            self.app.logger.error(error_msg)
            raise MySQLError(error_msg)

        return result

    def insert_query(self, cols, vals):
        """Inserts data into table of database

        First of all:
            DON'T CONVERT THE VALUES TO STRING BY YOURSELF!!!

        In this function, we would first create a template for a set of the data by the number of updated fields of
        the tables (e.g., len(cols)). Then, we can directly pass the input value if the vals is a 1-D list;
        otherwise, duplicating the templates by the numbers of data (e.g., len(vals)), and flatten the 2-D input values
        into 1-D list to be the arguments that we would like to pass to the cursor.execute(query, flatten_vals).
        In this way, we can prevent from sql injection and also can get better regular expression string parsing
        supported by pymysql module.

        Besides, if we need to update on duplicated key, we can use the following query syntax:
        INSERT INTO tbl_name (a,b,c) VALUES(1,2,3),(4,5,6) on duplicate key update b=values(b), c=values(c)

        Args:
            cols: A sequence of strings representing the fields of the table that we would like to update or insert.
            vals: A list that stores the data we would like to update or insert. If it is a 2-D list,
                the first dimension represent the number of the total data and the second dimension store the values of
                the field for each data.

        Local Variables:
            vals_set: A string representing the format of a single data. The string looks like '('%s','%s',...,'%s')'.
            vals_set_args: A string representing that represents how many set we would like to insert into the table
                of a database. The string looks like '('%s','%s',...,'%s'), ..., ('%s','%s',...,'%s')'.

        Returns:
           No return.

        Raises:
            MySQLError: An error occurred when query executing failed.
        """
        if not vals:
            return

        vals_set = "({})".format(",".join(['%s'] * len(cols)))
        if isinstance(vals[0], list):  # processing 2-D list of vals
            flatten_vals = [var for data in vals for var in data]
            vals_set_args = ",".join([vals_set] * len(vals))
        else:
            flatten_vals = vals
            vals_set_args = vals_set

        cols_str = ",".join(cols)
        reps_str = ",".join([c + "=values(" + c + ")" for c in cols])
        query = \
            "INSERT INTO {table} ({cols_str}) VALUES {vals_set_args} on duplicate key update {reps_str};"\
            .format(table=self.table, cols_str=cols_str, vals_set_args=vals_set_args, reps_str=reps_str)
        self.execute_query(query, flatten_vals)
