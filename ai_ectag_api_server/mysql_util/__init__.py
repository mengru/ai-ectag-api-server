
from ai_ectag_api_server.mysql_util.mysql_agent import MySQLAgent

ai_ectag_db_agent = MySQLAgent()


def init_ai_ectag_db(app, host, port, user, password, database, table, allow_modification):
    global ai_ectag_db_agent
    ai_ectag_db_agent = MySQLAgent(app, host, port, user, password, database, table, allow_modification)


def get_ai_ectag_db_agent():
    return ai_ectag_db_agent


def get_all_db_agents():
    return [get_ai_ectag_db_agent(), ]
