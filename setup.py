from setuptools import setup, find_packages

setup(
    name='ai_ectag_api_server',
    version='1.0',
    long_description=__doc__,
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        # use requirements.txt
    ],
    entry_points={
        "console_scripts": [
            "ai-ectag-api-server = ai_ectag_api_server.main:main",
        ],
    },
)
